suppressPackageStartupMessages({
  library(rgdax)
  library(dplyr)
  library(purrr)
  library(ggplot2)
  library(rpart)
  library(rpart.plot)
  library(tidyr)
  library(lubridate)
  library(quantmod)
  library(glue)
  library(logger)
})

devtools::load_all()

source("scripts/coinbot/coinbot_features.R")


# load historical prices --------------------------------------------------
log_info("download prices...")
btc_history <- get_chart_btc(from="2015-01-01", to=Sys.Date(), product="BTC-EUR")
nasdaq_history <- get_chart(symbols$NASDAQ)

log_info("download finished:
  ---- Bitcoin ----
  start: {min(btc_history$date)}
  end: {max(btc_history$date)}
  first close: {first(btc_history$close)}
  last close: {last(btc_history$close)}

  ---- NASDAQ ----
  start: {min(nasdaq_history$date)}
  end: {max(nasdaq_history$date)}
  first close: {first(nasdaq_history$close)}
  last close: {last(nasdaq_history$close)}
")


# feature engineering -----------------------------------------------------
log_info("feature engineering...")

# label: price change in 7 days
btc_label <- btc_history %>%
  inner_join(mutate(btc_history, date = date - 7), "date") %>%
  transmute(date, label = (close.y - close.x) / close.x)

# rolling features
nasdaq_features <- prep_features(nasdaq_history)
btc_features <- prep_features(btc_history)

# join data
full_data <- btc_label %>%
  inner_join(btc_features, "date") %>%
  left_join(nasdaq_features, "date", suffix = c("", "_nasdaq")) %>%
  fill(ends_with("_nasdaq"), .direction="up")


# train decision tree -----------------------------------------------------
log_info("train...")

set.seed(123)

train <- btc_train_nasdaq %>%
  filter(!is.na(label))

test <- btc_train_nasdaq %>%
  slice_tail(n = 1)

if (as.Date(test$time) <= Sys.Date()) {
  stop(glue("Letzter Eintrag ist nicht von gestern! as.Date(test$time) > Sys.Date() = {as.Date(test$time)} <= {Sys.Date()}"))
}

model <- rpart(label ~ . - close - time, train, control = rpart.control(minbucket = 15))
if (interactive()) {
  rpart.plot(model, cex=0.7)
}
pred <- predict(model, test)

if (!is.na(pred) & !(pred >= .95 & pred <= 1.05)) {
  balance <- get_balance()

  if(DEBUG) {
    stop("Debug-mode")
  }

  if (pred > 1) {
    # buy #
    transaction <- buy_btc(balance * .1)

    oo <- get_open_orders()
    i <- 0
    while (length(oo) > 0 && nrow(filter(oo, id == transaction$id))) {
      print("transaction is pending...")
      Sys.sleep(2)
      oo <- get_open_orders()
    }

    print(glue("Kaufe BTC `amount={transaction$size}`"))
  } else {
    # sell #
    transaction <- sell_btc(balance * .1)

    oo <- get_open_orders()
    i <- 0
    while (length(oo) > 0 && nrow(filter(oo, id == transaction$id))) {
      print("transaction is pending...")
      Sys.sleep(2)
      oo <- get_open_orders()
    }

    print(glue("Verkaufe BTC `amount={transaction$size}`"))
  }
} else {
  print(glue("Nichts getan. `pred={round(pred, 4)}`"))
}
